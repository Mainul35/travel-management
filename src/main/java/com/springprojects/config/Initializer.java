package com.springprojects.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.springprojects.entity.Authority;
import com.springprojects.entity.UserEntity;
import com.springprojects.service.AuthorityService;
import com.springprojects.service.UserService;

public class Initializer {

	public Initializer(AuthorityService authorityService, UserService userService, PasswordEncoder encoder) {
		// TODO Auto-generated constructor stub
	
		authorityService.create(new Authority(System.nanoTime(), "ROLE_ADMIN"));
		authorityService.create(new Authority(System.nanoTime(), "ROLE_LINE_MANAGER"));
		authorityService.create(new Authority(System.nanoTime(), "ROLE_SECURITY_DEPT"));
		authorityService.create(new Authority(System.nanoTime(), "ROLE_BC_TRAVEL_DESK"));
		authorityService.create(new Authority(System.nanoTime(), "ROLE_END_USER"));
		UserEntity userEntity = new UserEntity();
		userEntity.setId(System.nanoTime());
		userEntity.setName("Admin");
		userEntity.setEmail("teamg5.bit@gmail.com");
		userEntity.setDepartment("admin");
		Set<Authority> authorities = new HashSet<>();
		authorities.add(authorityService.findByRoleName("ROLE_ADMIN"));
		userEntity.setAuthorities(authorities);
		userEntity.setEnabled(true);
		userEntity.setPassword(encoder.encode("secret"));
		userEntity.setUsername("admin");

		userService.createUser(userEntity);

		
		//############################	 Insert Student manager data
		
		//Student 1
		
		userEntity = new UserEntity();
		userEntity.setId(System.nanoTime());
		userEntity.setName("Mainul Hasan");
		userEntity.setEmail("mainuls18@gmail.com");
		userEntity.setDepartment("BIT");
		authorities = new HashSet<>();
		authorities.add(authorityService.findByRoleName("ROLE_END_USER"));
		userEntity.setAuthorities(authorities);
		userEntity.setEnabled(true);
		userEntity.setPassword(encoder.encode("secret"));
		userEntity.setUsername("mainul35");

		userService.createUser(userEntity);
		
		
		//Student 2
		
		userEntity = new UserEntity();
		userEntity.setId(System.nanoTime());
		userEntity.setName("Sazzad Hossain");
		userEntity.setEmail("sazzadhossainsakib@gmail.com");
		userEntity.setDepartment("BIT");
		authorities = new HashSet<>();
		authorities.add(authorityService.findByRoleName("ROLE_SECURITY_DEPT"));
		userEntity.setAuthorities(authorities);
		userEntity.setEnabled(true);
		userEntity.setPassword(encoder.encode("secret"));
		userEntity.setUsername("sazzad");

		userService.createUser(userEntity);
		
		//User 3
		
		userEntity = new UserEntity();
		userEntity.setId(System.nanoTime());
		userEntity.setName("Tanveer Rahman");
		userEntity.setEmail("tanveershuvos@gmail.com");
		userEntity.setDepartment("BIT");
		authorities = new HashSet<>();
		authorities.add(authorityService.findByRoleName("ROLE_LINE_MANAGER"));
		userEntity.setAuthorities(authorities);
		userEntity.setEnabled(true);
		userEntity.setPassword(encoder.encode("secret"));
		userEntity.setUsername("tanveer");

		userService.createUser(userEntity);
		
		//QA Manager
		
		userEntity = new UserEntity();
		userEntity.setId(System.nanoTime());
		userEntity.setName("QA Manager");
		userEntity.setEmail("qa.manager.bit37@gmail.com");
		userEntity.setDepartment("QA");
		authorities = new HashSet<>();
		authorities.add(authorityService.findByRoleName("ROLE_BC_TRAVEL_DESK"));
		userEntity.setAuthorities(authorities);
		userEntity.setEnabled(true);
		userEntity.setPassword(encoder.encode("secret"));
		userEntity.setUsername("qa_manager");

		userService.createUser(userEntity);
	}
}
