package com.springprojects.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springprojects.entity.Area;
import com.springprojects.repository.AreaRepository;

@Service
public class AreaService {
	@Autowired
	private AreaRepository areaRepository;
	@Autowired
	private DestinationService destinationService;
	
	public String createOrUpdate(Area area) {
		Area area2 = areaRepository.findByAreaNameAndDestination(area.getAreaName(), area.getDestination());
		if(area2==null) {
			areaRepository.save(area);
			return "Created";
		}else {
			areaRepository.update(area2.getAreaId(), area.getAreaName(), area.getDestination());
			return "Updated";
		}
	}
	
	public List<Area> getAllAreas(){
		return areaRepository.findAll();
	}
	
	public void delete(Area area) {
		areaRepository.delete(area);
	}

	public List<Area> getAreasByDestinationName(String destinationName) {
		// TODO Auto-generated method stub
		return  areaRepository.findByDestination(destinationService.findByName(destinationName));
	}

	public Area findByName(String areaName) {
		// TODO Auto-generated method stub
		return areaRepository.findByAreaName(areaName);
	}

	public Area getAreaById(Long areaId) {
		// TODO Auto-generated method stub
		return areaRepository.findOne(areaId);
	}
}
