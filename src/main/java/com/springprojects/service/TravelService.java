package com.springprojects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springprojects.entity.Travel;
import com.springprojects.repository.TravelReposiory;

@Service
public class TravelService {
	@Autowired
	private TravelReposiory travelRepository;

	public void save(Travel travel) {
		// TODO Auto-generated method stub
		travelRepository.save(travel);
	}

}
