package com.springprojects.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springprojects.entity.Destination;
import com.springprojects.repository.DestinationRepository;

@Service
public class DestinationService {

	@Autowired
	DestinationRepository destinationRepository;
	
	public String createOrUpdate(Destination destination) {
		if(destinationRepository.findByDestinationName(destination.getDestinationName())==null) {
			destinationRepository.save(destination);
			return "Created";
		}else {
			destinationRepository.update(destination.getDestinationId(), destination.getDestinationName());
			return "Updated";
		}
	}
	
	public List<Destination> getAllDestinations(){
		return destinationRepository.findAll();
	}

	public Destination findByName(String destinationName) {
		// TODO Auto-generated method stub
		return destinationRepository.findByDestinationName(destinationName);
	}
}
