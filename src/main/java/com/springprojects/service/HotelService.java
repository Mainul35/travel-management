package com.springprojects.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springprojects.entity.Area;
import com.springprojects.entity.Hotel;
import com.springprojects.repository.HotelRepository;

@Service
public class HotelService {

	@Autowired
	private HotelRepository hotelRepository;

	public String saveOrUpdate(Hotel hotel) {
		Hotel hotel2 = hotelRepository.findByHotelNameAndLocation(hotel.getHotelName(), hotel.getLocation());
		if(hotel2==null) {
			hotelRepository.save(hotel);
			return "Created";
		}else {
			hotelRepository.update(hotel2.getHotelId(), hotel.getHotelName(), hotel.getLat(), hotel.getLocation(), hotel.getLon());
			return "Updated";
		}
	}
	public List<Hotel> getAllHotels() {
		// TODO Auto-generated method stub
		return hotelRepository.findAll();
	}
	
	public void remove(Hotel hotel) {
		hotelRepository.delete(hotel);
	}
	public List<Hotel> getHotelsByLocation(Area area) {
		// TODO Auto-generated method stub
		return hotelRepository.findByLocation(area);
	}
	
	
}
