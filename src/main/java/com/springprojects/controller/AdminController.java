package com.springprojects.controller;

import com.springprojects.config.Utils;
import com.springprojects.entity.Area;
import com.springprojects.entity.Authority;
import com.springprojects.entity.Destination;
import com.springprojects.entity.Hotel;
import com.springprojects.entity.UserEntity;
import com.springprojects.service.AreaService;
import com.springprojects.service.AuthorityService;
import com.springprojects.service.DestinationService;
import com.springprojects.service.HotelService;
import com.springprojects.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userService;
	@Autowired
	private AuthorityService authorityService;
	@Autowired
	private DestinationService destinationService;
	@Autowired
	private Utils utils;

	private Logger logger = Logger.getLogger(AdminController.class.getName());
	@Autowired
	private AreaService areaService;
	@Autowired
	private HotelService hotelService;

	@RequestMapping("/dashboard")
	public String dashboard(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		logger.info("Admin Dashboard : ");
		return "/admin_template/index";
	}

	@RequestMapping("/main-admin-navigations")
	public String mainNavigationsOfAdmin_GET(Model model, HttpSession session) {
		logger.info("Admin Navigations : ");
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		return "/includes/main_admin_navigations";
	}

	@RequestMapping(value = "/set-terms-and-conditions", method = RequestMethod.GET)
	public String setTermsAndConditions_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		String tNc = utils.readFile("/TermsAndConditions.txt");
		model.addAttribute("tNc", tNc);
		model.addAttribute("pageName", "terms-and-conditions");
		return "/admin_template/index";
	}

	@RequestMapping(value = "/set-terms-and-conditions", method = RequestMethod.POST)
	public String setTermsAndConditions_POST(Model model, HttpSession session, @RequestParam("editor1") String tc) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		model.addAttribute("pageName", "terms-and-conditions");
		tc = utils.writeFile("/TermsAndConditions.txt", tc);
		model.addAttribute("tNc", tc);
		return "/admin_template/index";
	}

	/**
	 * 
	 * Destination management code
	 * 
	 */
	@RequestMapping(value = "/create-destination", method = RequestMethod.GET)
	public String createDestination_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("msg", "");
		model.addAttribute("usr", userEntity);
		model.addAttribute("destination", new Destination());
		logger.info("Admin -> create-destination[GET] : ");
		return "/admin_template/create_destination";
	}

	@RequestMapping(value = "/create-destination", method = RequestMethod.POST)
	public String createDestination_POST(Model model, HttpSession session,
			@ModelAttribute("destination") Destination destination) {
		if (null == destination.getDestinationName() || destination.getDestinationName().isEmpty()) {
			return "redirect:/admin/create-destination";
		}
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		destination.setDestinationId(System.currentTimeMillis());
		String status = destinationService.createOrUpdate(destination);
		model.addAttribute("msg", status + " successfully.");
		model.addAttribute("usr", userEntity);
		model.addAttribute("destination", new Destination());
		logger.info("Admin -> create-destination[POST] : ");

		return "/admin_template/create_destination";
	}

	@RequestMapping(value = "/view-destinations", method = RequestMethod.GET)
	public String viewDestinations_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("msg", "");
		model.addAttribute("usr", userEntity);
		model.addAttribute("destinations", destinationService.getAllDestinations());
		logger.info("Admin -> view-destinations[GET] : ");

		return "/admin_template/view_all_destinations";
	}

	/**
	 * 
	 * Area Management code
	 * 
	 */

	@RequestMapping(value = "/create-visiting-area", method = RequestMethod.GET)
	public String createVisitingArea_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("msg", "");
		model.addAttribute("usr", userEntity);
		List<String> destinationNames = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination -> {
			String destinationName = destination.getDestinationName();
			destinationNames.add(destinationName);
		});
		model.addAttribute("destinationList", destinationNames);
		List<Area> areas = new ArrayList<Area>();
		if (areaService.getAllAreas() != null) {
			areas = areaService.getAllAreas();
		}
		model.addAttribute("areas", areas);
		model.addAttribute("visitingArea", new Area());
		logger.info("Admin -> create-visiting-area[GET] : ");

		return "/admin_template/create_visiting_area";
	}

	@RequestMapping(value = "/create-visiting-area", method = RequestMethod.POST)
	public String createVisitingArea_GET(Model model, HttpSession session,
			@RequestParam(name = "destinationName", defaultValue = "") String destName,
			@ModelAttribute("visitingArea") Area area) {
		if (null == destName || destName.isEmpty() || null == area.getAreaName() || area.getAreaName().isEmpty()) {
			return "redirect:/admin/create-visiting-area";
		}
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		area.setAreaId(System.currentTimeMillis());
		area.setDestination(destinationService.findByName(destName));
		String status = areaService.createOrUpdate(area);
		model.addAttribute("msg", status + " successfully.");
		model.addAttribute("usr", userEntity);
		List<String> destinationNames = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination -> {
			String destinationName = destination.getDestinationName();
			destinationNames.add(destinationName);
		});
		model.addAttribute("destinationList", destinationNames);
		model.addAttribute("visitingArea", new Area());
		logger.info("Admin -> create-visiting-area[POST] : ");

		return "/admin_template/create_visiting_area";
	}

	@RequestMapping(value = "/view-visiting-areas", method = RequestMethod.GET)
	public String viewVisitingAreas_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		model.addAttribute("areas", areaService.getAllAreas());
		logger.info("Admin -> view-visiting-areas[GET] : ");

		return "/admin_template/view_all_areas";
	}

	/**
	 * 
	 * Hotel Management Code
	 * 
	 */

	@RequestMapping(value = "/create-hotel", method = RequestMethod.GET)
	public String createHotel_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("msg", "");
		model.addAttribute("usr", userEntity);
		List<String> destinationNames = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination -> {
			String destinationName = destination.getDestinationName();
			destinationNames.add(destinationName);
		});
		model.addAttribute("destinationList", destinationNames);
		model.addAttribute("hotel", new Hotel());
		logger.info("Admin -> create-hotel[GET] : ");

		return "/admin_template/create_hotel";
	}

	@RequestMapping(value = "/get-areas-by-destination/{destinationName}")
	@ResponseBody
	public List<Area> getAreasByName_GET(@PathVariable("destinationName") String destinationName) {
		List<Area> areas = areaService.getAreasByDestinationName(destinationName);
		return areas;
	}

	@RequestMapping(value = "/create-hotel", method = RequestMethod.POST)
	public String createHotel_GET(Model model, HttpSession session,
			@RequestParam(name = "areaName", defaultValue = "") String areaName, @ModelAttribute("hotel") Hotel hotel) {
		if (null == areaName || areaName.isEmpty() || null == hotel.getHotelName() || hotel.getHotelName().isEmpty()) {
			return "redirect:/admin/create-hotel";
		}
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		hotel.setHotelId(System.currentTimeMillis());
		hotel.setLocation(areaService.findByName(areaName));
		String status = hotelService.saveOrUpdate(hotel);
		model.addAttribute("msg", status + " successfully.");
		model.addAttribute("usr", userEntity);
		List<String> destinationNames = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination -> {
			String destinationName = destination.getDestinationName();
			destinationNames.add(destinationName);
		});
		model.addAttribute("destinationList", destinationNames);
		model.addAttribute("hotel", new Hotel());
		logger.info("Admin -> create-hotel[POST] : ");

		return "/admin_template/create_hotel";
	}

	@RequestMapping(value = "/view-all-hotels", method = RequestMethod.GET)
	public String viewHotels_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		model.addAttribute("hotels", hotelService.getAllHotels());
		logger.info("Admin -> view-all-hotels[GET] : ");

		return "/admin_template/view_all_hotels";
	}

	@RequestMapping(value = "/create-role", method = RequestMethod.POST)
	public String createRole_POST(Model model, HttpSession session, @ModelAttribute("role") Authority role) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		model.addAttribute("pageName", "create-role");

		role.setId(System.currentTimeMillis());
		if (authorityService.create(role) == null) {
			model.addAttribute("isOk", "false");
		} else {
			model.addAttribute("isOk", "true");
		}
		model.addAttribute("role", role);
		return "/admin_template/index";
	}

	@RequestMapping(value = "/view-all-roles", method = RequestMethod.GET)
	public String viewAllRoles_GET(Model model, HttpSession session) {
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		model.addAttribute("usr", userEntity);
		model.addAttribute("pageName", "view-all-roles");
		model.addAttribute("roles", authorityService.listAllAuthorities());
		return "/admin_template/index";
	}

	// @RequestMapping(value = "/create-tag", method = RequestMethod.GET)
	// public String createTag_GET(Model model, HttpSession session) {
	// UserEntity userEntity = (UserEntity) session.getAttribute("usr");
	// model.addAttribute("usr", userEntity);
	// model.addAttribute("pageName", "create-tag");
	// model.addAttribute("tag", new Tag());
	// return "/admin_template/index";
	// }
	//
	// @RequestMapping(value = "/create-tag", method = RequestMethod.POST)
	// public String createTag_POST(Model model, HttpSession session,
	// @ModelAttribute("tag") Tag tag,
	// @RequestParam(name = "opening-date", required = true) String openingDate,
	// @RequestParam(name = "closure-date", required = true) String closureDate,
	// @RequestParam(name = "final-closure-date", required = true, defaultValue =
	// "") String finalClosureDate) {
	//
	// UserEntity userEntity = (UserEntity) session.getAttribute("usr");
	// model.addAttribute("usr", userEntity);
	// model.addAttribute("pageName", "create-tag");
	//
	// tag.setTagId(System.currentTimeMillis());
	// System.out.println(openingDate);
	// tag.setOpeningDate(utils.convertStringToTimestamp(openingDate + " 00:00:00",
	// "dd/MM/yyyy HH:mm:ss"));
	// tag.setClosingDate(utils.convertStringToTimestamp(closureDate + " 00:00:00",
	// "dd/MM/yyyy HH:mm:ss"));
	//// if (finalClosureDate.equals("")) {
	//// SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
	//// Calendar c = Calendar.getInstance();
	//// System.out.println(Integer.parseInt(closureDate.split("/")[2]) + " "
	//// + Integer.parseInt(closureDate.split("/")[1]) + " " +
	// Integer.parseInt(closureDate.split("/")[0]));
	//// c.set(Calendar.YEAR, Integer.parseInt(closureDate.split("/")[2]));
	//// c.set(Calendar.MONTH, Integer.parseInt(closureDate.split("/")[1]));
	//// c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(closureDate.split("/")[0]));
	//// c.add(Calendar.DATE, 15); // Adding 5 days
	//// String output = sdf.format(c.getTime());
	////
	//// System.out.println(LocalDateTime.from(c.getTime().toInstant().atZone(ZoneId.of("UTC"))).plusDays(1));
	////
	//// System.out.println(output);
	//// tag.setFinalClosingDate(utils.convertDateTimeToTimestamp(output + "
	// 00:00:00", "dd/MM/yyyy HH:mm:ss"));
	//// } else {
	// tag.setFinalClosingDate(
	// utils.convertStringToTimestamp(finalClosureDate + " 00:00:00", "dd/MM/yyyy
	// HH:mm:ss"));
	//// }
	//
	// System.out.println(tag.getOpeningDate() + "\t" + tag.getClosingDate() + "\t"
	// + tag.getFinalClosingDate());
	// if (tagService.save(tag) == false) {
	// model.addAttribute("isOk", "false");
	// } else {
	// model.addAttribute("isOk", "true");
	// }
	// model.addAttribute("tag", tag);
	// return "/admin_template/index";
	// }

	// @RequestMapping(value = "/view-all-tags", method = RequestMethod.GET)
	// public String viewAllTags_GET(Model model, HttpSession session) {
	// UserEntity userEntity = (UserEntity) session.getAttribute("usr");
	// model.addAttribute("usr", userEntity);
	// model.addAttribute("pageName", "view-all-tags");
	// model.addAttribute("tags", tagService.listAllTags());
	// return "/admin_template/index";
	// }
}
