package com.springprojects.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springprojects.config.Utils;
import com.springprojects.customModel.EndUserData;
import com.springprojects.entity.Travel;
import com.springprojects.entity.UserEntity;
import com.springprojects.service.DestinationService;
import com.springprojects.service.TravelService;

@Controller
public class TravelController {
	private Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private Utils utils;
	@Autowired
	private TravelService travelService;
	@Autowired
	private DestinationService destinationService;
	
	@RequestMapping(method = RequestMethod.GET, value = {"/request-travel", "/end-user/dashboard"})
	public String requestTravel_GET(Model model, HttpSession session) {

		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		
		model.addAttribute("usr", userEntity);
		List<String> destinations = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination->{
			String destinationName = destination.getDestinationName();
			System.out.println(destinationName);
			destinations.add(destinationName);
		});
		EndUserData endUserData = new EndUserData();
		List<String> hotels = new ArrayList<>();
		hotels.add("Radisson");
		hotels.add("Meridian");
		List<String> personList = new ArrayList<>();
		personList.add("Mainul");
		personList.add("Hasan");
		endUserData.setDestinations(destinations);
		endUserData.setHotels(hotels);
		endUserData.setPersonList(personList);
		model.addAttribute("endUserData", endUserData);
		logger.info("End User -> Request Travel : ");
		return "/end_user/request_travel";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/end-user/inside-dhaka"})
	public String insideDhaka_GET(Model model, HttpSession session) {
		
		logger.info("End User -> Request Travel -> Inside Dhaka : ");
		return "/end_user/inside_dhaka";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {"/end-user/outside-dhaka"})
	public String outsideDhaka_GET(Model model, HttpSession session) {
		
		logger.info("End User -> Request Travel -> Outside Dhaka : ");
		return "/end_user/outside_dhaka";
	}
	

	@RequestMapping(method = RequestMethod.POST, value = {"/request-travel", "/end-user/dashboard"})
	public String requestTravel_POST(
			Model model, 
			HttpSession session, 
			@RequestParam(name="travel-type", defaultValue="") String travelType,
			@RequestParam(name="travel-plan", defaultValue="") String travelPlan,
			@RequestParam(name="mode-of-transport", defaultValue="") String modeOfTransport,
			@RequestParam(name="uk-staff-name", defaultValue="") String ukStaffName,
			@RequestParam(name="meeting-type", defaultValue="") String meetingType,
			@RequestParam(name="crowd-involved", defaultValue="") String crowdInvolved,
			@RequestParam(name="travelling-date", defaultValue="") String travellingDate,
			@RequestParam(name="returning-date", defaultValue="") String returningDate,
			@RequestParam(name="transport-type", defaultValue="") String transportType,
			@RequestParam(name="transport-requirement-date", defaultValue="") String transportationDate,
			@RequestParam(name="transport-requirement-time", defaultValue="") String transportationTime,
			@RequestParam(name="requested-for", defaultValue="") String requestedFor,
			@ModelAttribute("endUserData") EndUserData endUserData
			) {
		
		endUserData.getTravel().setTravelId(System.currentTimeMillis());
		endUserData.getTravel().setTravelType(travelType);
		endUserData.getTravel().setTravelPlan(travelPlan);
		endUserData.getTravel().setModeOfTransport(modeOfTransport);
		endUserData.getTravel().setUkStaffName(ukStaffName);
		endUserData.getTravel().setMeetingType(meetingType);
		endUserData.getTravel().setCrowdInvolved(crowdInvolved);
		endUserData.getTravel().setTravellingDate(utils.convertStringToTimestamp(travellingDate, "dd/MM/yyyy"));
		endUserData.getTravel().setReturningDate(utils.convertStringToTimestamp(returningDate, "dd/MM/yyyy"));
		endUserData.getTravel().setTransportationType(transportType);
		endUserData.getTravel().setTransportationDateTime(utils.convertStringToTimestamp(transportationDate+" "+transportationTime, "MM/dd/yyyy HH:mm a"));
		endUserData.getTravel().setRequestedFor(requestedFor);
		UserEntity userEntity = (UserEntity) session.getAttribute("usr");
		endUserData.getTravel().setRequestedBy(userEntity);
		List<String> destinations = new ArrayList<>();
		destinationService.getAllDestinations().forEach(destination->{
			String destinationName = destination.getDestinationName();
			System.out.println(destinationName);
			destinations.add(destinationName);
		});
		List<String> hotels = new ArrayList<>();
		hotels.add("Radisson");
		hotels.add("Meridian");
		List<String> personList = new ArrayList<>();
		personList.add("Mainul");
		personList.add("Hasan");
		endUserData.setDestinations(destinations);
		endUserData.setHotels(hotels);
		endUserData.setPersonList(personList);
		
		model.addAttribute("endUserData", endUserData);
		model.addAttribute("usr", userEntity);
//		System.out.println(endUserData.getTravel().toString());
		travelService.save(endUserData.getTravel());
		logger.info("End User -> Request Travel : ");
		return "/end_user/request_travel";
	}

}
