package com.springprojects.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springprojects.entity.Area;
import com.springprojects.entity.Hotel;

@Repository
@Transactional
public interface HotelRepository extends JpaRepository<Hotel, Long>{

	@Modifying
	@Query("update Hotel h set h.hotelName = ?2, h.lat = ?3, h.location=?4, h.lon=5 where h.hotelId = ?1")
	void update(Long hotelId, String hotelName, Double lat, Area location, Double lon);

	Hotel findByHotelNameAndLocation(String hotelName, Area location);

	List<Hotel> findByLocation(Area location);

}
