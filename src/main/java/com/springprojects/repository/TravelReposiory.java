package com.springprojects.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.springprojects.entity.Travel;

@Service
public interface TravelReposiory extends JpaRepository<Travel, Long>{

}
