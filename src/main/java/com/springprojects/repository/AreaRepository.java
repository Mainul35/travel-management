package com.springprojects.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springprojects.entity.Area;
import com.springprojects.entity.Destination;

@Transactional
@Repository
public interface AreaRepository extends JpaRepository<Area, Long> {

	Area findByAreaNameAndDestination(String areaName, Destination destination);
	
	@Modifying
	@Query("update Area a set a.areaName = ?2, a.destination = ?3 where a.areaId = ?1")
	void update(Long areaId, String areaName, Destination destination);

	List<Area> findByDestination(Destination destination);

	Area findByAreaName(String areaName);

}
