package com.springprojects.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springprojects.entity.Area;
import com.springprojects.entity.Destination;

@Repository
@Transactional
public interface DestinationRepository extends JpaRepository<Destination, Long> {
	
	@Modifying
	@Query("update Destination d set d.destinationName = ?2 where d.destinationId = ?1")
	void update(Long destinationId, String destinationName);

	Destination findByDestinationName(String destinationName);
}
