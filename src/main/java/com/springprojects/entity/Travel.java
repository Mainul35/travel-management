package com.springprojects.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class Travel {
	@Id
	@Column(name = "travel_id")
	private Long travelId;
	@Column(name = "destination")
	private String destination;
	@Column(name="purpose")
	private String purpose;
	@Column(name="visiting_area")
	private String visitingArea;
	@Column(name="travel_type")
	private String travelType;
	@Column(name="travel_plan")
	private String travelPlan;
	@Column(name="mode_of_transport")
	private String modeOfTransport;
	@Column(name="uk_staff_name")
	private String ukStaffName;
	@Column(name="meeting_type")
	private String meetingType;
	@Column(name="is_crowd_involved")
	private String crowdInvolved;
	@Column(name="wbs_code")
	private String wbsCode;
	@Column(name="travel_date")
	private Timestamp travellingDate;
	@Column(name="return_date")
	private Timestamp returningDate;
	@Column(name="preffered_hotel")
	private String prefferedHotel;
	@Column(name="transportatio_type")
	private String transportationType;
	@Column(name="transportation_date_time")
	private Timestamp transportationDateTime;
	@OneToOne
	private UserEntity requestedBy;
	@Column(name="requested_for")
	private String requestedFor;
	@Lob
	@Column(name="remark")
	private String remark;
	public Long getTravelId() {
		return travelId;
	}
	public void setTravelId(Long travelId) {
		this.travelId = travelId;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getVisitingArea() {
		return visitingArea;
	}
	public void setVisitingArea(String visitingArea) {
		this.visitingArea = visitingArea;
	}
	public String getTravelType() {
		return travelType;
	}
	public void setTravelType(String travelType) {
		this.travelType = travelType;
	}
	public String getTravelPlan() {
		return travelPlan;
	}
	public void setTravelPlan(String travelPlan) {
		this.travelPlan = travelPlan;
	}
	public String getModeOfTransport() {
		return modeOfTransport;
	}
	public void setModeOfTransport(String modeOfTransport) {
		this.modeOfTransport = modeOfTransport;
	}
	public String getUkStaffName() {
		return ukStaffName;
	}
	public void setUkStaffName(String ukStaffName) {
		this.ukStaffName = ukStaffName;
	}
	public String getMeetingType() {
		return meetingType;
	}
	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}
	public String getCrowdInvolved() {
		return crowdInvolved;
	}
	public void setCrowdInvolved(String crowdInvolved) {
		this.crowdInvolved = crowdInvolved;
	}
	public String getWbsCode() {
		return wbsCode;
	}
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}
	public Timestamp getTravellingDate() {
		return travellingDate;
	}
	public void setTravellingDate(Timestamp travellingDate) {
		this.travellingDate = travellingDate;
	}
	public Timestamp getReturningDate() {
		return returningDate;
	}
	public void setReturningDate(Timestamp returningDate) {
		this.returningDate = returningDate;
	}
	public String getPrefferedHotel() {
		return prefferedHotel;
	}
	public void setPrefferedHotel(String prefferedHotel) {
		this.prefferedHotel = prefferedHotel;
	}
	public String getTransportationType() {
		return transportationType;
	}
	public void setTransportationType(String transportationType) {
		this.transportationType = transportationType;
	}
	public Timestamp getTransportationDateTime() {
		return transportationDateTime;
	}
	public void setTransportationDateTime(Timestamp transportationDateTime) {
		this.transportationDateTime = transportationDateTime;
	}
	public UserEntity getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(UserEntity requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getRequestedFor() {
		return requestedFor;
	}
	public void setRequestedFor(String requestedFor) {
		this.requestedFor = requestedFor;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crowdInvolved == null) ? 0 : crowdInvolved.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((meetingType == null) ? 0 : meetingType.hashCode());
		result = prime * result + ((modeOfTransport == null) ? 0 : modeOfTransport.hashCode());
		result = prime * result + ((prefferedHotel == null) ? 0 : prefferedHotel.hashCode());
		result = prime * result + ((purpose == null) ? 0 : purpose.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((requestedBy == null) ? 0 : requestedBy.hashCode());
		result = prime * result + ((requestedFor == null) ? 0 : requestedFor.hashCode());
		result = prime * result + ((returningDate == null) ? 0 : returningDate.hashCode());
		result = prime * result + ((transportationDateTime == null) ? 0 : transportationDateTime.hashCode());
		result = prime * result + ((transportationType == null) ? 0 : transportationType.hashCode());
		result = prime * result + ((travelId == null) ? 0 : travelId.hashCode());
		result = prime * result + ((travelPlan == null) ? 0 : travelPlan.hashCode());
		result = prime * result + ((travelType == null) ? 0 : travelType.hashCode());
		result = prime * result + ((travellingDate == null) ? 0 : travellingDate.hashCode());
		result = prime * result + ((ukStaffName == null) ? 0 : ukStaffName.hashCode());
		result = prime * result + ((visitingArea == null) ? 0 : visitingArea.hashCode());
		result = prime * result + ((wbsCode == null) ? 0 : wbsCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Travel other = (Travel) obj;
		if (crowdInvolved == null) {
			if (other.crowdInvolved != null)
				return false;
		} else if (!crowdInvolved.equals(other.crowdInvolved))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (meetingType == null) {
			if (other.meetingType != null)
				return false;
		} else if (!meetingType.equals(other.meetingType))
			return false;
		if (modeOfTransport == null) {
			if (other.modeOfTransport != null)
				return false;
		} else if (!modeOfTransport.equals(other.modeOfTransport))
			return false;
		if (prefferedHotel == null) {
			if (other.prefferedHotel != null)
				return false;
		} else if (!prefferedHotel.equals(other.prefferedHotel))
			return false;
		if (purpose == null) {
			if (other.purpose != null)
				return false;
		} else if (!purpose.equals(other.purpose))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (requestedBy == null) {
			if (other.requestedBy != null)
				return false;
		} else if (!requestedBy.equals(other.requestedBy))
			return false;
		if (requestedFor == null) {
			if (other.requestedFor != null)
				return false;
		} else if (!requestedFor.equals(other.requestedFor))
			return false;
		if (returningDate == null) {
			if (other.returningDate != null)
				return false;
		} else if (!returningDate.equals(other.returningDate))
			return false;
		if (transportationDateTime == null) {
			if (other.transportationDateTime != null)
				return false;
		} else if (!transportationDateTime.equals(other.transportationDateTime))
			return false;
		if (transportationType == null) {
			if (other.transportationType != null)
				return false;
		} else if (!transportationType.equals(other.transportationType))
			return false;
		if (travelId == null) {
			if (other.travelId != null)
				return false;
		} else if (!travelId.equals(other.travelId))
			return false;
		if (travelPlan == null) {
			if (other.travelPlan != null)
				return false;
		} else if (!travelPlan.equals(other.travelPlan))
			return false;
		if (travelType == null) {
			if (other.travelType != null)
				return false;
		} else if (!travelType.equals(other.travelType))
			return false;
		if (travellingDate == null) {
			if (other.travellingDate != null)
				return false;
		} else if (!travellingDate.equals(other.travellingDate))
			return false;
		if (ukStaffName == null) {
			if (other.ukStaffName != null)
				return false;
		} else if (!ukStaffName.equals(other.ukStaffName))
			return false;
		if (visitingArea == null) {
			if (other.visitingArea != null)
				return false;
		} else if (!visitingArea.equals(other.visitingArea))
			return false;
		if (wbsCode == null) {
			if (other.wbsCode != null)
				return false;
		} else if (!wbsCode.equals(other.wbsCode))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Travel [travelId=" + travelId + ", destination=" + destination + ", purpose=" + purpose
				+ ", visitingArea=" + visitingArea + ", travelType=" + travelType + ", travelPlan=" + travelPlan
				+ ", modeOfTransport=" + modeOfTransport + ", ukStaffName=" + ukStaffName + ", meetingType="
				+ meetingType + ", crowdInvolved=" + crowdInvolved + ", wbsCode=" + wbsCode + ", travellingDate="
				+ travellingDate + ", returningDate=" + returningDate + ", prefferedHotel=" + prefferedHotel
				+ ", transportationType=" + transportationType + ", transportationDateTime=" + transportationDateTime
				+ ", requestedBy=" + requestedBy + ", requestedFor=" + requestedFor + ", remark=" + remark + "]";
	}
}
