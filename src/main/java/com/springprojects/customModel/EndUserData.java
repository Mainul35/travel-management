package com.springprojects.customModel;

import java.util.List;

import com.springprojects.entity.Travel;

public class EndUserData {
	
	private List<String> destinations;
	private List<String> hotels;
	private List<String> personList;
	private Travel travel;
	public List<String> getDestinations() {
		return destinations;
	}
	public void setDestinations(List<String> destinations) {
		this.destinations = destinations;
	}
	public List<String> getHotels() {
		return hotels;
	}
	public void setHotels(List<String> hotels) {
		this.hotels = hotels;
	}
	public List<String> getPersonList() {
		return personList;
	}
	public void setPersonList(List<String> personList) {
		this.personList = personList;
	}
	public Travel getTravel() {
		return travel;
	}
	public void setTravel(Travel travel) {
		this.travel = travel;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destinations == null) ? 0 : destinations.hashCode());
		result = prime * result + ((hotels == null) ? 0 : hotels.hashCode());
		result = prime * result + ((personList == null) ? 0 : personList.hashCode());
		result = prime * result + ((travel == null) ? 0 : travel.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EndUserData other = (EndUserData) obj;
		if (destinations == null) {
			if (other.destinations != null)
				return false;
		} else if (!destinations.equals(other.destinations))
			return false;
		if (hotels == null) {
			if (other.hotels != null)
				return false;
		} else if (!hotels.equals(other.hotels))
			return false;
		if (personList == null) {
			if (other.personList != null)
				return false;
		} else if (!personList.equals(other.personList))
			return false;
		if (travel == null) {
			if (other.travel != null)
				return false;
		} else if (!travel.equals(other.travel))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EndUserData [destinations=" + destinations + ", hotels=" + hotels + ", personList=" + personList
				+ ", travel=" + travel + "]";
	}

}
